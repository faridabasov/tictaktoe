package com.ingress.tictaktoe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TictaktoeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TictaktoeApplication.class, args);
	}

}
