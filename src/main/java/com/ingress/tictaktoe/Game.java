package com.ingress.tictaktoe;

import java.util.Arrays;
import java.util.List;

public class Game {
        public static void main(String[] args){
            System.out.println("12345678hfenfjene");
        Game game = new Game();
            game.play(2,0);
            game.play(2,2);
            game.play(1,1);
            game.play(1,2);
            game.play(0,2);
    }
    public char nextFlag = 'X';
    private char[][] board = new char[][]{{'\0', '\0', '\0'},
            {'\0', '\0', '\0'},
            {'\0', '\0', '\0'}};

    String play(int xCor, int yCor) {
        checkCorBorder(xCor, yCor);
        checkPosition(xCor, yCor);
        createPostion(xCor, yCor);
        showBoard();
        if (isWin()) return nextFlag + " is WINNER";

        if(isDraw()) return "GAME IS DRAW";
        changeFlag();
        System.out.println("-----------------------------");
        return "NO WINNER";
    }

    private void checkCorBorder(int xCor, int yCor) {
        if (xCor >= 0 && xCor > 2) {
            throw new RuntimeException("XCor is outside the table");
        }
        if (yCor >= 0 && yCor > 2) {
            throw new RuntimeException("YCor is outside the table");
        }
    }

    private void checkPosition(int xCor, int yCor) {
        if (board[xCor][yCor] != '\0') {
            throw new RuntimeException("board is occupied");
        }
    }

    private void createPostion(int xCor, int yCor) {
        board[xCor][yCor] = nextFlag;
    }

    private boolean isWin() {
        if (checkVerticalWinner() ||
                checkHorizontalWinner() ||
                checkDiagonalWinner())
            return true;
        return false;
    }

    private boolean checkVerticalWinner(){
        int total = nextFlag*3;
        for(int i = 0; i<3; i++){
            if(total == board[i][0]+board[i][1]+board[i][2])
                return true;
        }
        return false;
    }
    private boolean checkHorizontalWinner(){
        int total = nextFlag*3;
        for(int i = 0; i<3; i++){
            if(total == board[0][i]+board[1][i]+board[2][i])
                return true;
        }
        return false;
    }
    private boolean checkDiagonalWinner(){
        int total = nextFlag*3;
        int sumForward = 0;
        int sumBack = 0;
        for(int i = 0; i<3; i++){
            sumForward += board[i][i];
            sumBack += board[i][2-i];
        }
        if(total == sumForward || total == sumBack){
            return true;
        }
        return false;
    }

    private boolean isDraw(){

        for (int i = 0; i<3;i++){
            for (int j = 0; j<3;j++){
                if(board[i][j]=='\0'){
                    return false;
                }
            }
        }
        return true;
    }






    private void changeFlag() {
        if (nextFlag == 'X')
            nextFlag = 'Y';
        else
            nextFlag = 'X';
    }


    private void showBoard() {
        System.out.println(Arrays.deepToString(board));
    }
}
