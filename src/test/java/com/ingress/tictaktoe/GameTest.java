package com.ingress.tictaktoe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.*;

@ExtendWith(MockitoExtension.class)
public class GameTest {

    @InjectMocks
    Game game;

    @Test
    void whenXcorOutSideofTable() {
        assertThatThrownBy(() -> game.play(4, 0))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("XCor is outside the table");
    }

    @Test
    void whenYcorOutSideofTable() {
        assertThatThrownBy(() -> game.play(0, 4))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("YCor is outside the table");
    }

    @Test
    void boardIsOccupied() {
        game.play(0, 0);
        assertThatThrownBy(() -> game.play(0, 0))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("board is occupied");
    }

    @Test
    void firstPlayerIsX(){
        assertThat(game.nextFlag).isEqualTo('X');
    }

    @Test
    void secondPlayerIsY(){
        game.play(0, 0);
        assertThat(game.nextFlag).isEqualTo('Y');
    }

    @Test
    void noWinner(){
        assertThat(game.play(1,1)).isEqualTo("NO WINNER");
    }

    @Test
    void isHorizontalXWinner(){
        game.play(1,0);
        game.play(2,2);
        game.play(1,1);
        game.play(2,1);
        assertThat(game.play(1,2)).isEqualTo("X is WINNER");
    }

    @Test
    void isHorizontalYWinner(){
        game.play(0,0);
        game.play(2,2);
        game.play(0,1);
        game.play(2,1);
        game.play(1,1);
        assertThat(game.play(2,0)).isEqualTo("Y is WINNER");
    }

    @Test
    void isVerticalXWinner(){
        game.play(0,1);
        game.play(2,2);
        game.play(1,1);
        game.play(2,0);
        assertThat(game.play(2,1)).isEqualTo("X is WINNER");
    }

    @Test
    void isVerticalYWinner(){
        game.play(0,0);
        game.play(2,2);
        game.play(0,1);
        game.play(1,2);
        game.play(1,1);
        assertThat(game.play(0,2)).isEqualTo("Y is WINNER");
    }

    @Test
    void isForwardDiogonalXWinner(){
        game.play(0,0);
        game.play(2,0);
        game.play(1,1);
        game.play(1,2);
        assertThat(game.play(2,2)).isEqualTo("X is WINNER");
    }

    @Test
    void isForwardDiogonalYWinner(){
        game.play(0,2);
        game.play(2,2);
        game.play(0,1);
        game.play(1,1);
        game.play(1,2);
        assertThat(game.play(0,0)).isEqualTo("Y is WINNER");
    }

    @Test
    void isBackDiogonalXWinner(){
        game.play(2,0);
        game.play(2,2);
        game.play(1,1);
        game.play(1,2);
        assertThat(game.play(0,2)).isEqualTo("X is WINNER");
    }

    @Test
    void isBackDiogonalYWinner(){
        game.play(0,0);
        game.play(2,0);
        game.play(2,2);
        game.play(1,1);
        game.play(1,2);
        assertThat(game.play(0,2)).isEqualTo("Y is WINNER");
    }

    @Test
    void isDrow(){
        game.play(0,0);
        game.play(0,1);
        game.play(0,2);
        game.play(1,0);
        game.play(1,1);
        game.play(2,0);
        game.play(2,1);
        game.play(2,2);
        assertThat(game.play(1,2)).isEqualTo("GAME IS DRAW");
    }
}
